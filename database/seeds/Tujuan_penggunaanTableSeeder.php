<?php

use Illuminate\Database\Seeder;

class Tujuan_penggunaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__. '/../csv/tujuan_penggunaan.csv';
        $data = csv_to_array($file, ['keterangan']);
        $city = DB::table('tujuan_penggunaan')->where('keterangan', $data[0]['keterangan'])->first();
        if ( !$city ) {
            foreach(collect($data)->chunk(50) as $chunk) {
                \DB::table('tujuan_penggunaan')->insert($chunk->toArray());
            }
        }
    }
}
