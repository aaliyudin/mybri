<?php

use Illuminate\Database\Seeder;

class Jenis_pinjamanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__. '/../csv/jenis_pinjaman.csv';
        $data = csv_to_array($file, ['keterangan']);
        $city = DB::table('jenis_pinjaman')->where('keterangan', $data[0]['keterangan'])->first();
        if ( !$city ) {
            foreach(collect($data)->chunk(50) as $chunk) {
                \DB::table('jenis_pinjaman')->insert($chunk->toArray());
            }
        }
    }
}
