<?php

use Illuminate\Database\Seeder;

class Pendidikan_terakhirTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__. '/../csv/pendidikan_terakhir.csv';
        $data = csv_to_array($file, ['keterangan']);
        $city = DB::table('pendidikan_terakhir')->where('keterangan', $data[0]['keterangan'])->first();
        if ( !$city ) {
            foreach(collect($data)->chunk(50) as $chunk) {
                \DB::table('pendidikan_terakhir')->insert($chunk->toArray());
            }
        }
    }
}
