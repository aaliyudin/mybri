<?php

use Illuminate\Database\Seeder;

class MitraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__. '/../csv/mitra.csv';
        $data = csv_to_array($file, ['keterangan']);
        $city = DB::table('mitra')->where('keterangan', $data[0]['keterangan'])->first();
        if ( !$city ) {
            foreach(collect($data)->chunk(50) as $chunk) {
                \DB::table('mitra')->insert($chunk->toArray());
            }
        }
    }
}
