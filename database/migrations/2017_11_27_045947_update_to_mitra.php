<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToMitra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        	 Schema::table( 'mitra', function ( Blueprint $table ) {
              $table->text( 'instansi' )->nullable();

        } );
		 DB::table('mitra')->insert(
        array(
	    'kode' => '31',
            'instansi' => '00001',
            'keterangan' => 'Pilih',
        )
		);
         DB::table('mitra')->insert(
		array(
            'kode' => '32',
            'instansi' => '00002',
            'keterangan' => 'Mitra1',
        )
		);
         DB::table('mitra')->insert(
		array(
            'kode' => '33',
            'instansi' => '00003',
            'keterangan' => 'Mitra2',
        )
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
