<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MitraRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create( 'mitra_relation', function ( Blueprint $table ) {
            $table->increments( 'kode' );
			$table->text( 'BRANCH_CODE' )->nullable();
			$table->text( 'NAMA_INSTANSI' )->nullable();
			$table->text( 'idMitrakerja' )->nullable();
			$table->text( 'segmen' )->nullable();
			$table->foreign( 'kode' )
                ->references( 'kode' )->on( 'mitra_relation' );
        } );
		
		
         DB::table('mitra_relation')->insert(
        array(
            'BRANCH_CODE' => '00001',
            'NAMA_INSTANSI' => 'AJENDAM XVI PATTIMURA',
            'idMitrakerja' => 'Err:511',
            'segmen' => 'MIKRO',
        )
		);
         DB::table('mitra_relation')->insert(
		array(
            'BRANCH_CODE' => '00001',
            'NAMA_INSTANSI' => 'BABIMINVETCAD XVI PATTIMURA',
            'idMitrakerja' => 'Err:511',
            'segmen' => 'MIKRO',
        )
		);
         DB::table('mitra_relation')->insert(
		array(
            'BRANCH_CODE' => '00001',
            'NAMA_INSTANSI' => 'BALAI ARKEOLOGI AMBON',
            'idMitrakerja' => 'Err:511',
            'segmen' => 'MIKRO',
        )
		);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
