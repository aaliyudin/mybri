<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertMitra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::table('mitra')->insert(
        array(
            'kode' => '1',
            'keterangan' => 'Pilih',
        )
		);
         DB::table('mitra')->insert(
		array(
            'kode' => '2',
            'keterangan' => 'Mitra1',
        )
		);
         DB::table('mitra')->insert(
		array(
            'kode' => '3',
            'keterangan' => 'Mitra2',
        )
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
