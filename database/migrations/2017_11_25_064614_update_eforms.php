<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEforms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
       Schema::table( 'briguna', function ( Blueprint $table ) {
            $table->text( 'NPWP' )->nullable();
			$table->text( 'NIP' )->nullable();
			$table->text( 'Status_Pekerjaan' )->nullable();
			$table->text( 'Nama_atasan_Langsung' )->nullable();
			$table->text( 'Jabatan_atasan' )->nullable();
			$table->text( 'SK_PERTAMA' )->nullable();
			$table->text( 'SK_TERAKHIR' )->nullable();
			$table->text( 'REKOMENDASI_ATASAN' )->nullable();
        } );
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table( 'eforms', function ( Blueprint $table ) {
            $table->dropColumn( 'NPWP' );
			$table->dropColumn( 'NIP' );
			$table->dropColumn( 'Status_Pekerjaan' );
			$table->dropColumn( 'Nama_atasan_Langsung' );
			$table->dropColumn( 'Jabatan_atasan' );
			$table->dropColumn( 'SK_PERTAMA' );
			$table->dropColumn( 'SK_TERAKHIR' );
			$table->dropColumn( 'REKOMENDASI_ATASAN' );			
        } );
    }
}
