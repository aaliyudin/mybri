<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pn')->unsigned()->index();
            $table->string('activity_object');
            $table->string('action_type');
            $table->string('start_date');
            $table->string('end_date');
            $table->text('longitude');
            $table->text('latitude');
            $table->integer('marketing_id')->unsigned()->index();
            $table->foreign('marketing_id')
              ->references('id')
              ->on('marketings')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->string('pn_join');
            $table->string('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
