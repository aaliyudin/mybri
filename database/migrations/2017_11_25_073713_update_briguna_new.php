<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBrigunaNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'briguna', function ( Blueprint $table ) {
			$table->text( 'NPWP_NASABAH' )->nullable();
			$table->text( 'KARTU_KELUARGA' )->nullable();
			$table->text( 'SLIP_GAJI' )->nullable();
        } );
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	 Schema::table( 'briguna', function ( Blueprint $table ) {
            $table->dropColumn( 'NPWP' );
			$table->dropColumn( 'NIP' );
			$table->dropColumn( 'Status_Pekerjaan' );
			$table->dropColumn( 'Nama_atasan_Langsung' );
			$table->dropColumn( 'Jabatan_atasan' );
			$table->dropColumn( 'NPWP_NASABAH' );
			$table->dropColumn( 'KARTU_KELUARGA' );
			$table->dropColumn( 'SLIP_GAJI' );
			$table->dropColumn( 'SK_PERTAMA' );
			$table->dropColumn( 'SK_TERAKHIR' );
			$table->dropColumn( 'REKOMENDASI_ATASAN' );			
        } );

    }
}
