 <table align="center" bgcolor="#fafafa" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td align="center">
            <table align="center" class="table-inner" width="500px" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <img align="center" width="200px" src="https://mybri.stagingapps.net/assets/images/logo/Logo-Website.png">
                    </td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td align="center"><u><b>SURAT REKOMENDASI ATASAN</b></u></td>
                </tr>
                <tr>
                    <td align="center"><b>No.</b></td>
                </tr>
                <tr>
                    <td bgcolor="#F7941E" height="5" align="center"></td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td align="left">
                        <span>Yang bertanda tangan dibawah ini, !</span>
                        <br>
						<table>
							<tr>
								<td>Nama</td>
								<td>:</td>
							</tr>
							<tr>
								<td>NIP</td>
								<td>:</td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td>:</td>
							</tr>
							<tr>
								<td>Instansi/Perusahaan</td>
								<td>:</td>
							</tr>
						</table>
						<br>
					<span>Dengan ini menerangkan, bahwa :</span>
                        <br>
						<table>
							<tr>
								<td>Nama</td>
								<td>:</td>
							</tr>
							<tr>
								<td>NIP</td>
								<td>:</td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td>:</td>
							</tr>
							<tr>
								<td>Instansi/Perusahaan</td>
								<td>:</td>
							</tr>
						</table>
						<br>
					<span>adalah pegawai dengan status Pegawai Tetap pada : ...............................................................,dan</span>
                        <br>
					<span>Ybs selama ini berkerja dengan baik.</span>
                        <br>
					<span>Kami tidak berkeberatan <b>pegawai</b> yang namanya tersebut diatas mengajukan pinjaman BRIGUNA kepada PT. Bank Rakyat Indonesia (Persero) TBK</span>
                        <br>
                    <td align="right">
					<span>..........................................................</span>
					<br>
					<br>
					<br>
					<span>..........................................................</span>
					<br>
					<span>..........................................................</span>
                    	
					</td>
						
						
				
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td align="center" bgcolor="#fafafa">
            <table class="textbutton" align="center" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td align="center">
                         
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#fafafa">
            <img align="center" width="500" src="https://mybri.stagingapps.net/assets/images/logo/footer.png">
        </td>
    </tr>
</table>