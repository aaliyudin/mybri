 <table align="center" bgcolor="#fafafa" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td align="center">
            <table align="center" class="table-inner" width="500px" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <img align="center" width="200px" src="https://mybri.stagingapps.net/assets/images/logo/Logo-Website.png">
                    </td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td align="center">Selamat Bergabung</td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td bgcolor="#F7941E" height="5" align="center"></td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td align="center">
                        <span>Hai, {!! $mail['name'] !!}!</span>
                        <br>
                        Selamat bergabung di My BRI.
                        <br>
                        Kemudahan anda untuk investasi properti Anda,
                        <br>
                        berikut informasi akun Anda di My BRI :
                        <br>
                        <table width="100%" border="1">
                        	<tr>
                        		<td>Email : </td>
                        		<td>{!! $mail['email'] !!}</td>
                        	</tr>
                        	<tr>
                        		<td>Password : </td>
                        		<td>{!! $mail['password'] !!}</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td align="center" bgcolor="#fafafa">
            <img align="center" width="500" src="https://mybri.stagingapps.net/assets/images/logo/footer.png">
        </td>
    </tr>
</table>