 <table align="center" bgcolor="#fafafa" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td align="center">
            <table align="center" class="table-inner" width="500px" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <img align="center" width="200px" src="https://mybri.stagingapps.net/assets/images/logo/Logo-Website.png">
                    </td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td align="center">Aktivasi Akun Anda</td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td bgcolor="#F7941E" height="5" align="center"></td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
                <tr>
                    <td align="center">
                        <span>Hai, {!! $mail['name'] !!}!</span>
                        <br>
                        Terima kasih telah mendaftarkan akun Anda di My BRI.
                        <br>
                        Untuk langkah selanjutnya, silahkan meng-klik tombol di bawah ini untuk mengaktivasi akun Anda.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td align="center" bgcolor="#fafafa">
            <table class="textbutton" align="center" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td align="center">
                            <a href="{!! $mail[ 'url' ] !!}">Tautan</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#fafafa">
            <img align="center" width="500" src="https://mybri.stagingapps.net/assets/images/logo/footer.png">
        </td>
    </tr>
</table>