<?php

namespace App\Http\Requests\API\v1\Crm\Activity;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'activity_object' => 'required',
          'action_type' => 'required',
          'start_date' => 'required',
          'end_date' => 'required',
          'longitude' => 'required',
          'latitude' => 'required',
          'marketing_id' => 'required',
          'pn_join' => 'required',
          'desc' => 'required'
        ];
    }
}
