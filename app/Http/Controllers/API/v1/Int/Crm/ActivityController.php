<?php

namespace App\Http\Controllers\API\v1\Int\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\API\v1\Crm\Activity\CreateRequest;
use App\Http\Requests\API\v1\Crm\Activity\UpdateRequest;
use App\Models\Crm\Activity;
use App\Models\Crm\Marketing;
use App\Models\Crm\ActivityType;
use App\Models\Crm\ProductType;
use App\Models\Crm\Status;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $pn = $request->header('pn');
      $activities = Activity::where('pn',$pn)->get();
      return response()->success( [
          'message' => 'Sukses',
          'contents' => $activities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $pn = $request->header('pn');
      $data['marketings'] = Marketing::where('pn', $pn)->get();
      $data['officer'] = 'list officer';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
      $postTaken = [
        'activity_object',
        'action_type',
        'start_date',
        'end_date',
        'longitude',
        'latitude',
        'marketing_id',
        'pn_join',
        'desc'
      ];

      $save = Activity::create($request->only($postTaken));

      if ($save) {
        return response()->success([
          'message' => 'Data Activity berhasil ditambah.',
          'contents' => collect($save)->merge($request->all()),
        ], 201);
      }

      return response()->error([
        'message' => 'Data Activity Tidak Dapat Ditambah.',
      ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reSchedule($id){
      //Reschedule Activity
    }
}
