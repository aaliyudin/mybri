<?php

namespace App\Models;

use App\Events\Customer\CustomerRegistered;
use Illuminate\Database\Eloquent\Builder;
use App\Models\CustomerDetail;
use App\Models\User;
use Sentinel;

class Customer extends User
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [ 'is_simple', 'is_completed', 'is_verified', 'personal', 'work', 'financial', 'contact', 'other', 'schedule', 'is_approved' ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'is_simple', 'is_completed', 'is_verified', 'personal', 'work', 'financial', 'contact', 'other', 'schedule', 'is_approved' ];

    /**
     * Get information about register simple status.
     *
     * @return string
     */
    public function getIsSimpleAttribute()
    {
        return ! empty( $this->detail );
    }

    /**
     * Get information about register complete status.
     *
     * @return bool
     */
    public function getIsCompletedAttribute()
    {
        if ($this->detail) {
            $detail = $this->detail->toArray();
            if( $detail[ 'status' ] != 1 ) {
                $detail = array_diff_key( $detail, array_flip( [
                    'couple_nik', 'couple_name', 'couple_birth_date', 'couple_birth_place_id', 'couple_identity'
                ] ) );
            }
            $total_data = count( $detail );
            $filled = array_filter( $detail, function( $var ) {
                return $var !== NULL && $var !== '';
            } );
            $total_filled_data = count( $filled );
            if( $total_data - $total_filled_data == 0 ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get customer data status.
     *
     * @return bool
     */
    public function getIsVerifiedAttribute()
    {
        return $this->detail ? $this->detail->is_verified : false;
    }

    /**
     * Get personal information of customer.
     *
     * @return bool
     */
    public function getPersonalAttribute()
    {
        $personal_data = [
            'user_id' => $this->detail ? $this->detail->user_id : '',
            'name' => $this->fullname,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'gender' => $this->gender,
            'phone' => $this->phone,
            'mobile_phone' => $this->mobile_phone,
            'email' => $this->email,
            'nik' => $this->detail ? $this->detail->nik : '',
            'birth_place_id' => $this->detail ? $this->detail->birth_place_id : '',
            'birth_place' => $this->birth_place,
            'birth_date' => $this->detail ? $this->detail->birth_date : '',
            'address' => $this->detail ? $this->detail->address : '',
            'current_address' => $this->detail ? $this->detail->current_address : '',
            'city_id' => $this->detail ? $this->detail->city_id : '',
            'city' => $this->detail ? ($this->detail->city ? $this->detail->city->name : '') : '',
            'citizenship_id' => $this->detail ? $this->detail->citizenship_id : '',
            'citizenship' => $this->detail ? $this->detail->citizenship_name : '',
            'status' => $this->detail ? $this->detail->status : '',
            'address_status_id'=> $this->detail ? $this->detail->address_status_id : '',
            'address_status' => $this->detail ? $this->detail->address_status : '',
            'mother_name' => $this->detail ? $this->detail->mother_name : '',
            'couple_name' => $this->detail ? $this->detail->couple_name : '',
            'couple_nik' => $this->detail ? $this->detail->couple_nik : '',
            'couple_birth_date' => $this->detail ? $this->detail->couple_birth_date : '',
            'couple_birth_place_id' => $this->detail ? $this->detail->couple_birth_place_id : '',
            'couple_birth_place' => $this->couple_birth_place,
            'couple_identity' => $this->detail ? $this->detail->couple_identity : '',
            'status_id' => $this->detail ? $this->detail->status_id : '',
            'cif_number'=> $this->detail ? $this->detail->cif_number : '',
            'pendidikan_terakhir' => $this->detail ? $this->detail->pendidikan_terakhir : '',
            'address_domisili' => $this->detail ? $this->detail->address_domisili : '',
            'mobile_phone_couple' => $this->detail ? $this->detail->mobile_phone_couple : '',
        ];

        return $personal_data;
    }

    /**
     * Get work information of customer.
     *
     * @return bool
     */
    public function getWorkAttribute()
    {
        return [
            'type_id' => $this->detail ? $this->detail->job_type_id : '',
            'type' => $this->detail ? $this->detail->job_type_name : '',
            'work_id' => $this->detail ? $this->detail->job_id : '',
            'work' => $this->detail ? $this->detail->job_name : '',
            'company_name' => $this->detail ? $this->detail->company_name : '',
            'work_field_id' => $this->detail ? $this->detail->job_field_id : '',
            'work_field' => $this->detail ? $this->detail->job_field_name : '',
            'position_id' => $this->detail ? $this->detail->position : '',
            'position' => $this->detail ? $this->detail->position_name : '',
            'work_duration' => $this->detail ? $this->detail->work_duration : '',
            'work_duration_month' => $this->detail ? $this->detail->work_duration_month : '',
            'office_address' => $this->detail ? $this->detail->office_address : ''
        ];
    }

    /**
     * Get financial information of customer.
     *
     * @return bool
     */
    public function getFinancialAttribute()
    {
        return [
            'salary' => $this->detail ? $this->detail->salary : '',
            'other_salary' => $this->detail ? $this->detail->other_salary : '',
            'loan_installment' => $this->detail ? $this->detail->loan_installment : '',
            'dependent_amount' => $this->detail ? $this->detail->dependent_amount : '',
            'status_income' => $this->detail ? ($this->detail->couple_salary == NULL ? 'Pisah Harta':'Gabung Harta') : NULL,
            'status_finance' => $this->detail ? ($this->detail->couple_salary == NULL ? 'Single Income':'Join Income') : NULL,
            'salary_couple' => $this->detail ? $this->detail->couple_salary : '',
            'other_salary_couple' => $this->detail ? $this->detail->couple_other_salary : '',
            'loan_installment_couple' => $this->detail ? $this->detail->couple_loan_installment : ''
        ];
    }

    /**
     * Get contact information of customer.
     *
     * @return bool
     */
    public function getContactAttribute()
    {
        return [
            'emergency_contact' => $this->detail ? $this->detail->emergency_contact : '',
            'emergency_relation' => $this->detail ? $this->detail->emergency_relation : '',
            'emergency_name' => $this->detail ? $this->detail->emergency_name : ''
        ];
    }

    /**
     * Get other information of customer.
     *
     * @return bool
     */
    public function getOtherAttribute()
    {
        return [
            'image' => $this->image,
            'identity' => $this->detail ? $this->detail->identity : '',
            'npwp' => $this->detail ? $this->detail->npwp : '',
            'family_card' => $this->detail ? $this->detail->family_card : '',
            'marrital_certificate' => $this->detail ? $this->detail->marrital_certificate : '',
            'diforce_certificate' => $this->detail ? $this->detail->diforce_certificate : '',
        ];
    }

    /**
     * Get other information of customer.
     *
     * @return bool
     */
    public function getScheduleAttribute()
    {
        $schedules = [];
        $eforms = $this->eforms()->select( [ 'appointment_date', 'ao_id', 'branch_id' ] )->where( 'appointment_date', '>=', date( 'Y-m-d' ) )->get();
        foreach ( $eforms as $key => $eform ) {
            $schedules[] = [
                'date' => $eform->appointment_date,
                'ao_name' => $eform->ao_name,
                'branch' => $eform->branch_id,
                'agenda' => ''
            ];
        }
        return $schedules;
    }

    /**
     * Get status is_approved.
     *
     * @return bool
     */

    public function getIsApprovedAttribute()
    {
        $stat_approved = [];
        $eform = $this->eforms()->select(['is_approved'])->get();
        foreach ($eform as $key => $stat) {
            \Log::info($stat->is_approved);
            $stat_approved = [
                'status' => $stat->is_approved
            ];
        }

        return $stat_approved;
    }

    /**
     * Get customer branch name.
     *
     * @return bool
     */
    public function getBirthPlaceAttribute()
    {
        if ($this->detail) {
            if( $this->detail->birth_place_city ) {
                return $this->detail->birth_place_city->name;
            }
        }
        return '';
    }

    /**
     * Get customer branch name.
     *
     * @return bool
     */
    public function getCoupleBirthPlaceAttribute()
    {
        if ($this->detail) {
            if( $this->detail->couple_birth_place_city ) {
                return $this->detail->couple_birth_place_city->name;
            }
        }
        return '';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function create( $data ) {
    	\Log::info('==========ini data awal=======');
    	\Log::info($data);
        $user_model = new User;
        $password = str_random( 8 );
        $separate_array_keys = array_flip( $user_model->fillable );
        $user_data = array_intersect_key( $data, $separate_array_keys ) + [ 'password' => $password ];
        \Log::info('============ini data setelah pabeulit=======');
    	\Log::info($user_data);
	    $user = Sentinel::registerAndActivate( $user_data );
        $role = Sentinel::findRoleBySlug( 'customer' );
        $role->users()->attach( $user );
	    \Log::info('==========ini data separate====');
	    \Log::info($separate_array_keys);

        if ( $data['status'] == 2 ) {
            $customer_data = [ 'user_id' => $user->id, 'identity'=> $data['identity'], 'couple_identity'=> $data['couple_identity'] ] + array_diff_key( $data, $separate_array_keys );

        } else {
            $customer_data = [ 'user_id' => $user->id, 'identity'=> $data['identity'] ] + array_diff_key( $data, $separate_array_keys );

        }

      	\Log::info('==========ini data insert ke detail=======');
	    \Log::info($customer_data);
        CustomerDetail::create( $customer_data );
        // send mail notification
        $customer = static::find( $user->id );
        event( new CustomerRegistered( $customer, $password ) );

        return $customer;
    }

    /**
     * Update the model in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     */
    public function update( array $attributes = [], array $options = [] )
    {
        $keys = array('npwp', 'identity', 'couple_identity', 'legal_document', 'salary_slip', 'bank_statement', 'family_card', 'marrital_certificate', 'diforce_certificate');

        $separate_array_keys = array_flip( $this->fillable );
        $user_data = array_intersect_key( $attributes, $separate_array_keys );
        parent::update( $user_data );
        $separate_array_keys = array_flip( $this->fillable );
        $customer_data = array_diff_key( $attributes, $separate_array_keys );
        unset( $customer_data[ '_method' ] );
        if (count($customer_data) > 0) {
          $this->detail()->update( $customer_data );
        }
        if ($this->detail) {
            $this->detail->updateAllImageAttribute( $keys, $customer_data, 'customer' );
        }

        foreach ($keys as $key) {
            if ( isset($data[ $key ]) ) {
                unset( $data[ $key ] );
            }
        }

        return true;
    }

    /**
     * verify customer data.
     *
     * @return void
     */
    public function verify( $data )
    {
        if( $data[ 'verify_status' ] == 'verify' ) {
            $data[ 'birth_date' ] = date( 'Y-m-d', strtotime( $data[ 'birth_date' ] ) );

            if (isset( $data[ 'couple_birth_date' ] )) {
                $data[ 'couple_birth_date' ] = date( 'Y-m-d', strtotime( $data[ 'couple_birth_date' ] ) );
            }

            $data[ 'gender' ] = str_replace( 'PEREMPUAN', 'P', $data[ 'gender' ] );
            $data[ 'gender' ] = str_replace( 'LAKI-LAKI', 'L', $data[ 'gender' ] );
            $data[ 'gender' ] = str_replace( 'Perempuan', 'P', $data[ 'gender' ] );
            $data[ 'gender' ] = str_replace( 'Laki-Laki', 'L', $data[ 'gender' ] );
            $data['emergency_contact'] = $data['emergency_mobile_phone'];
            $this->update( array_except( $data, ['emergency_mobile_phone','form_id','email','verify_status', '_method'] ) );
        } else if( $data[ 'verify_status' ] == 'verified' ) {
            $this->detail()->update( [
                'is_verified' => true
                , 'response_status' => 'approve'
            ] );
        }
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope( 'role', function( Builder $builder ) {
            $builder->whereHas( 'roles', function( $role ) {
                $role->whereSlug( 'customer' );
            } );
        } );
    }



    /**************************************************************************************
     *
     * Relationship functions
     *
     **************************************************************************************/

    /**
     * The directories belongs to broadcasts.
     *
     * @return     \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function detail()
    {
        return $this->hasOne( CustomerDetail::class, 'user_id' );
    }

    /**
     * The directories belongs to broadcasts.
     *
     * @return     \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eforms()
    {
        return $this->hasMany( EForm::class, 'user_id' );
    }
}
