<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
  protected $table = 'marketing_activities';
  /**
   * Fields that can be mass assigned.
   *
   * @var array
   */
   protected $fillable = [
     'pn', 
     'activity_type',
     'action_type',
     'start_date',
     'end_date',
     'longitude',
     'latitude',
     'marketing_id',
     'fo_join',
     'desc'
      ];

    /**
    * Fields that can be mass assigned.
    *
    * @var array
    */
    protected $hidden = ['created_at','updated_at'];
}
