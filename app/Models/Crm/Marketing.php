<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
  /**
   * Fields that can be mass assigned.
   *
   * @var array
   */
    protected $fillable = ['product_type', 'marketing_type', 'activity_type', 'pn', 'account', 'target', 'status', 'target_closing_date'];

  /**
   * Fields that can be mass assigned.
   *
   * @var array
   */
    protected $hidden = ['created_at', 'updated_at'];
}
